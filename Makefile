ARCHS = armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = Segue
Segue_FILES = Tweak.x
Segue_FRAMEWORKS = QuartzCore

include $(THEOS_MAKE_PATH)/tweak.mk
