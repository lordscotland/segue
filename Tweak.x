@interface PUSlideshowSettings
+(PUSlideshowSettings*)settingsForAirplayRoute:(id)route;
-(NSString*)transition;
@end

@interface CAFilter : NSObject
+(id)filterWithType:(NSString*)type;
@end

@interface CATransition (Private)
@property(assign) unsigned int transitionFlags;
@end

const NSString* kTransitionDataKey=@"_transitionData";
const NSString* kTransitionViewKey=@"transition view";
const NSString* kPreviousViewKey=@"previous view";
const NSString* kNextViewKey=@"next view";

static NSURL* $_rootURL;

%hook PLPhotoBrowserController
-(void)_slideshowWillBegin {
  %orig;
  NSString* tname=[%c(PUSlideshowSettings) settingsForAirplayRoute:nil].transition;
  if([tname.pathExtension isEqualToString:@"plist"]){
    NSMutableArray* transitions=[NSMutableArray
     arrayWithContentsOfURL:[$_rootURL URLByAppendingPathComponent:tname]];
    if(transitions.count && [[transitions objectAtIndex:0] isEqual:@"random"]){
      [transitions removeObjectAtIndex:0];
      objc_setAssociatedObject(transitions,&$_rootURL,(id)kCFBooleanTrue,
       OBJC_ASSOCIATION_ASSIGN);
    }
    if(transitions.count){
      objc_setAssociatedObject(self,&$_rootURL,transitions,
       OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
  }
}
-(void)_slideshowWillEnd {
  %orig;
  objc_setAssociatedObject(self,&$_rootURL,nil,
   OBJC_ASSOCIATION_ASSIGN);
}
-(void)_performCATransition:(NSDictionary*)dict {
  NSDictionary* tdata=[dict objectForKey:kTransitionDataKey];
  CALayer* layer;
  if(tdata){
    layer=[[dict objectForKey:kTransitionViewKey] layer];
    objc_setAssociatedObject(layer,&$_rootURL,tdata,
     OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  }
  %orig;
  if(tdata){
    objc_setAssociatedObject(layer,&$_rootURL,nil,
     OBJC_ASSOCIATION_ASSIGN);
  }
}
%end

%hook CALayer
-(void)addAnimation:(CATransition*)animation forKey:(NSString*)key {
  NSDictionary* tdata=objc_getAssociatedObject(self,&$_rootURL);
  if(tdata && [animation isKindOfClass:[CATransition class]]){
    CAFilter* filter=[CAFilter filterWithType:[tdata objectForKey:@"type"]];
    [tdata enumerateKeysAndObjectsUsingBlock:^(NSString* key,id value,BOOL* stop){
      if([key isEqualToString:@"subtype"])
       animation.subtype=value;
      else if([key isEqualToString:@"duration"])
       animation.duration=[value doubleValue];
      else if([key isEqualToString:@"timingFunction"])
       animation.timingFunction=[CAMediaTimingFunction functionWithName:value];
      else if([key isEqualToString:@"transitionFlags"])
       animation.transitionFlags=[value intValue];
      else if([key characterAtIndex:0]=='.')
       [filter setValue:value forKey:[key substringFromIndex:1]];
    }];
    animation.filter=filter;
  }
  %orig;
}
%end

%hook UITransitionView
-(BOOL)transition:(int)transition fromView:(UIView*)fromView toView:(UIView*)toView {
  id delegate=[self delegate];
  NSMutableArray* transitions=objc_getAssociatedObject(delegate,&$_rootURL);
  if(transitions){
    NSDictionary* tdata;
    if(objc_getAssociatedObject(transitions,&$_rootURL)){
      tdata=[transitions objectAtIndex:arc4random_uniform(transitions.count)];
    }
    else {
      tdata=[transitions objectAtIndex:0];
      [transitions addObject:tdata];
      [transitions removeObjectAtIndex:0];
    }
    [toView removeFromSuperview];
    [delegate performSelector:@selector(_performCATransition:)
     withObject:[NSDictionary dictionaryWithObjectsAndKeys:
     self,kTransitionViewKey,fromView,kPreviousViewKey,toView,kNextViewKey,
     tdata,kTransitionDataKey,nil] afterDelay:0];
    return YES;
  }
  return %orig;
}
%end

%hook PUSlideshowSettingsViewController
-(NSDictionary*)_localTransitions {
  NSMutableDictionary* transitions=[NSMutableDictionary dictionaryWithDictionary:%orig];
  for (NSURL* URL in [[NSFileManager defaultManager]
   enumeratorAtURL:$_rootURL includingPropertiesForKeys:nil
   options:NSDirectoryEnumerationSkipsSubdirectoryDescendants
   |NSDirectoryEnumerationSkipsHiddenFiles errorHandler:NULL]){
    if([URL.pathExtension isEqualToString:@"plist"]){
      NSString* name=URL.lastPathComponent;
      [transitions setObject:[@"\U0001F3AC "
       stringByAppendingString:name.stringByDeletingPathExtension] forKey:name];
    }
  }
  return transitions;
}
%end

%ctor {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  $_rootURL=[[NSURL alloc] initWithString:@"Segue/"
   relativeToURL:[[NSFileManager defaultManager]
   URLForDirectory:NSApplicationSupportDirectory inDomain:NSLocalDomainMask
   appropriateForURL:nil create:NO error:NULL]];
  %init;
  [pool drain];
}
